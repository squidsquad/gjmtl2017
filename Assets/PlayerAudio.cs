﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAudio : MonoBehaviour {

	[SerializeField]
	AudioSource _soundSource;

	[SerializeField]
	List<AudioClip> _attackSounds;

	[SerializeField]
	List<AudioClip> _hurtSounds;

	[SerializeField]
	List<AudioClip> _shieldSounds;

	public void PlayAttack () {
		SelectRandomFromList(_attackSounds);
	}

	public void PlayHurt()
	{
		SelectRandomFromList(_hurtSounds);
	}

	public void PlayShield()
	{
		SelectRandomFromList(_shieldSounds);
	}

	void SelectRandomFromList(List<AudioClip> soundsToGrabFrom)
	{
		_soundSource.clip = soundsToGrabFrom[Random.Range(0, soundsToGrabFrom.Count - 1)];
		_soundSource.Play();
	}
}
