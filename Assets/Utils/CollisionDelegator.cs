﻿using ActionExtensions;
using UnityEngine;

public class CollisionDelegator : MonoBehaviour {

	public System.Action<GameObject, Collision> TriggerEnterEvent;
	public System.Action<GameObject, Collision> TriggerExitEvent;

	private void OnCollisionEnter(Collision collision)
	{
		TriggerEnterEvent.SafeInvoke(gameObject, collision);
	}

	private void OnCollisionExit(Collision collision)
	{
		TriggerExitEvent.SafeInvoke(gameObject, collision);
	}
}
