﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionConstraint : MonoBehaviour {
	public Transform Parent;
	
	void Update () {
		if (Parent != null)
		{
			transform.position = Parent.position;
		}
	}
}
