﻿using System;

namespace ActionExtensions
{

	public static class ActionExtensions
	{
		public static void SafeInvoke(this Action action)
		{
			if (action != null)
			{
				action.Invoke();
			}
		}

		public static void SafeInvoke<T>(this Action<T> action, T obj)
		{
			if (action != null)
			{
				action.Invoke(obj);
			}
		}

		public static void SafeInvoke<T, U>(this Action<T, U> action, T obj1, U obj2)
		{
			if (action != null)
			{
				action.Invoke(obj1, obj2);
			}
		}
	}
}
