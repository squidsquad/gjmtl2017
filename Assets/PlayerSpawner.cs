﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawner : MonoBehaviour {

    [SerializeField]
    private GameObject playerPrefab;
    private GameObject[] spawns;

    void Start()
    {
        SpawnPlayer(4);
    }

    public void SpawnPlayer(int nbPlayers)
    {
        spawns = GameObject.FindGameObjectsWithTag("Spawn");

        for(int i = 0; i < nbPlayers; i++)
        {
            Instantiate(playerPrefab, spawns[i].transform.position, spawns[i].transform.rotation);
        }

    }
}
