﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletSpawner : MonoBehaviour {

	[SerializeField]
	Bullet _bulletPrefab;

	[SerializeField]
	float _cooldownBetweenShots;

	[SerializeField]
	Transform _spawnPosition;

    [SerializeField]
    private int ammoCost = 25;
    private Stat _ammo;

	[SerializeField]
	private Animator animator;

	[SerializeField]
	PlayerAudio _playerAudio;


	float _timeSinceLastShot = 0;

    public void SetAmmo(Stat ammo)
    {
        _ammo = ammo;
    }

	public void LaunchBullet()
	{
        if (_cooldownBetweenShots > _timeSinceLastShot || _ammo.CurrentValue < ammoCost || Time.deltaTime == 0)
		{
			return;
		}

		var bullet = Instantiate(_bulletPrefab, _spawnPosition.position, _spawnPosition.rotation);

		bullet.SetBulletDirection(_spawnPosition.rotation * new Vector3(0.0f, 0.0f, 1.0f));
		bullet.RandomizeType();

        _ammo.CurrentValue -= ammoCost;
		_timeSinceLastShot = 0.0f;

		animator.SetTrigger("Attack_Trig");

		_playerAudio.PlayAttack();
	}

	private void Start()
	{
		_timeSinceLastShot = _cooldownBetweenShots;
	}

	private void Update()
	{
		_timeSinceLastShot += Time.deltaTime;
	}

}
