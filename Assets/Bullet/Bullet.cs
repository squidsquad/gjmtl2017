﻿using UnityEngine;
using ActionExtensions; 

public class Bullet : MonoBehaviour {

	[SerializeField]
	Rigidbody _rigidBody;

	[SerializeField]
	float _baseSpeedFactor;

	[SerializeField]
	AttackVisualsSelector _visualsSelector;

	[SerializeField]
	private int _damage;

	[SerializeField]
	private AudioClip _bulletSnuffSound;

	[SerializeField]
	private float _killBulletSpeedThreshold;

	public int Damage
	{
		get
		{
			return _damage;
		}
		set
		{
			_bulletVelocity *= (value / (float)_damage);
			_damage = value;
			OnDamageChanged.SafeInvoke(value);

			if(value <= 0)
			{
				DestroyBullet();
			}
		}
	}


	public AttackType AttackType;

	public System.Action<AttackType> OnAttackTypeChanged;

	public System.Action<int> OnDamageChanged;

	Vector3 _bulletVelocity;

	private void OnCollisionEnter(Collision collision)
	{
		var collidedObject = collision.collider;

		if (collidedObject.CompareTag("Obstacle"))
		{
			// Set bounce velocity
			_bulletVelocity = Vector3.Reflect(_bulletVelocity, collision.contacts[0].normal);
			// Set bounce strength
			Damage--;
		}
		else if (collidedObject.CompareTag("Shield"))
		{
			var shieldComponent = collidedObject.GetComponent<Shield>();
			if(shieldComponent != null)
			{
				if(shieldComponent.ShieldType == AttackType)
				{
					_bulletVelocity = Vector3.Reflect(_bulletVelocity, collision.contacts[0].normal);
					Damage *= 2;
					RandomizeType();
				}
				else
				{
					// ignore shield
					Physics.IgnoreCollision(collidedObject, GetComponent<Collider>());
				}
			}
		}
        else if ((collidedObject.CompareTag("Player")))
        {
            DestroyBullet();
        }
	}

	// Update is called once per frame
	public void SetBulletDirection(Vector3 direction)
	{
		var speedFactor = _baseSpeedFactor * Damage;
		_bulletVelocity = direction.normalized * speedFactor;
	}

	public void RandomizeType()
	{
		AttackType = (AttackType)Random.Range(0, 4);

		OnAttackTypeChanged.SafeInvoke(AttackType);

		_visualsSelector.ActivateVisuals(AttackType);
	}

	private void DestroyBullet()
	{
		var gameObject = new GameObject();
		var aS = gameObject.AddComponent<AudioSource>();
		aS.clip = _bulletSnuffSound;
		aS.Play();
		gameObject.AddComponent<PlaySoundTillDeath>();
		Destroy(this.gameObject);
	}

	private void LateUpdate()
	{
		_rigidBody.velocity = _bulletVelocity;
		if(_bulletVelocity.magnitude <= _killBulletSpeedThreshold)
		{
			DestroyBullet();
		}
		Quaternion lookDirection = new Quaternion();
		lookDirection.SetLookRotation(_rigidBody.velocity);
		_rigidBody.rotation = lookDirection;
	}
}
