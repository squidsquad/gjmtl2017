﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackVisualsSelector : MonoBehaviour {
	/// <summary>
	/// Place colors in order of Blue, Green, Orange, Red
	/// </summary>
	public GameObject[] _attackTypeVisuals;

	public void ActivateVisuals(AttackType type)
	{
		for (int i = 0; i < _attackTypeVisuals.Length; ++i)
		{
			_attackTypeVisuals[i].SetActive(i == (int)type);
		}
	}
}
