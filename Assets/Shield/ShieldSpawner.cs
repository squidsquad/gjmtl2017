﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldSpawner : MonoBehaviour {

	[SerializeField]
	Shield _shieldPrefab;

	[SerializeField]
	float _cooldownBetweenShields;

    [SerializeField]
    Transform _spawnPosition;

	[SerializeField]
	PlayerAudio _playerAudio;

	float _timeSinceLastShield;

	private Shield _currentShield;

    private Stat _breath;
    [SerializeField]
    private int breathCost;

	[SerializeField]
	private Animator animator;

	public void SetBreathStat(Stat breath)
    {
        _breath = breath;
    }

	public void SpawnShield(AttackType shieldType)
	{
		// There is still a shield up, ignore call
		if(_currentShield != null || _cooldownBetweenShields > _timeSinceLastShield || _breath.CurrentValue < breathCost || Time.deltaTime == 0)
		{
			return;
		}

		_currentShield = Instantiate(_shieldPrefab, _spawnPosition.position, _spawnPosition.rotation);

		_currentShield.ShieldType = shieldType;
		_currentShield.GetComponent<PositionConstraint>().Parent = _spawnPosition;
		_currentShield.ShieldOver += CurrentShieldDied;
        _breath.CurrentValue -= breathCost;

		animator.SetTrigger("Shield_Trig");
		_playerAudio.PlayShield();

	}

	private void Start()
	{
		_timeSinceLastShield = _cooldownBetweenShields;
	}

	private void Update()
	{
		_timeSinceLastShield += Time.deltaTime;
	}

	void CurrentShieldDied()
	{
		_currentShield.ShieldOver -= CurrentShieldDied;
		_currentShield = null;
		_timeSinceLastShield = 0.0f;
	}
}
