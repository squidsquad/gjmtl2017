﻿using UnityEngine;
using ActionExtensions;

public class Shield : MonoBehaviour {

	[SerializeField]
	float _maxDuration;

	float _currentLifespan = 0;

	public AttackType ShieldType = AttackType.BLUE;

	public System.Action ShieldOver;

	[SerializeField]
	AttackVisualsSelector _visualsSelector;

	private void Start()
	{
		_visualsSelector.ActivateVisuals(ShieldType);
	}

	private void Update()
	{
		if(_currentLifespan> _maxDuration)
		{
			DestroyShield();
		}
		else
		{
			 _currentLifespan += Time.deltaTime;
		}
	}

	private void DestroyShield()
	{
		ShieldOver.SafeInvoke();
		Destroy(gameObject);
	}
}
