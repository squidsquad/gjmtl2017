﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCameraRotation : MonoBehaviour {

	GameObject _camera;

	private void Start()
	{
		_camera = GameObject.FindGameObjectWithTag("MainCamera");
	}

	// Update is called once per frame
	void Update () {
		if (_camera != null)
			transform.rotation = _camera.transform.rotation;
	}
}
