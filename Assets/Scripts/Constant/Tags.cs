﻿using UnityEngine;
using System.Collections;

namespace Tags {

	public enum UITags {
		PlayingUI,
		CharacterSelectionUI,
		PauseUI,
		EndingUI,
		ConversationUI
	}

	public enum EntityTags {
		Player
	}
}
