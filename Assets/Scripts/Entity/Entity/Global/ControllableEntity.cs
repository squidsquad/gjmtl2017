﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

[Serializable]
public abstract class ControllableEntity : MovableEntity {

	private string horizontalAxis, verticalAxis, 
					aAxis, bAxis, xAxis, yAxis, 
					leftTriggerAxis, LeftBumperAxis, 
					rightTriggerAxis, rightBumperAxis;

    private int controllerNumber;
	private const string HORIZONTAL = "Horizontal" , VERTICAL = "Vertical", 
							A = "A" , X = "X", B = "B", Y = "Y", 
							LEFT_TRIGGER = "LeftTrigger" , LEFT_BUMPER = "LeftBumper", 
							RIGHT_TRIGGER = "RightTrigger" , RIGHT_BUMPER = "RightBumper";

	public abstract void defense1 ();
	public abstract void defense2 ();
	public abstract void defense3 ();
	public abstract void defense4 ();
	public abstract void attack ();

	public Animator Anim {
		get { return animator; }
	}

	public int ControllerNumber {
		get { return controllerNumber; }
		set { 
			this.controllerNumber = value; 
			horizontalAxis = HORIZONTAL + "_P" + value;
			verticalAxis = VERTICAL + "_P" + value;
			aAxis = A + "_P" + value;
			bAxis = B + "_P" + value;
			xAxis = X + "_P" + value;
			yAxis = Y + "_P" + value;
			leftTriggerAxis = LEFT_TRIGGER + "_P" + value;
			LeftBumperAxis = LEFT_BUMPER + "_P" + value;
			rightTriggerAxis = RIGHT_TRIGGER + "_P" + value;
			rightBumperAxis = RIGHT_BUMPER + "_P" + value;
		}
	}

    virtual protected void Update() {
		if (controllerNumber != 0) {

			float horizontal = Input.GetAxis (horizontalAxis);
			float vertical = Input.GetAxis (verticalAxis);
			// choose between acceleration and deceleration.
			Move(horizontal, vertical, Time.deltaTime);

			if (Input.GetButtonDown(aAxis)) {
				defense1 ();
			}
			if (Input.GetButtonDown(bAxis)) {
				defense2 ();
			}
			if (Input.GetButtonDown(xAxis)) {
				defense3 ();
			}
			if (Input.GetButtonDown(yAxis)) {
				defense4 ();
			}
			if (/*Input.GetAxis(leftTriggerAxis) != 0 
				|| Input.GetButtonDown(LeftBumperAxis) 
				|| Input.GetAxis(rightTriggerAxis) != 0 
				|| */Input.GetButtonDown(rightBumperAxis)) {
				attack ();
			}
		}
    }
}
