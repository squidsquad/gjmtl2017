﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public abstract class MovableEntity : Entity {

	public float MaxSpeed;
	public float CurrentSpeed;
	public float MoveAcceleration;
	public float RotateSpeed;

	[SerializeField]
	private CharacterController _characterControl;

	[SerializeField]
	protected Animator animator;

	Vector3 lastVelocityDirection;

	public void Move(float inputX, float inputY, float deltaTime) {

		if (deltaTime == 0)
		{
			return;
		}

		bool playerInput = (inputX != 0 || inputY != 0);

		float acceleration = (playerInput ? MoveAcceleration : -MoveAcceleration)*deltaTime;

		CurrentSpeed = Mathf.Clamp( CurrentSpeed + acceleration, 0.0f, MaxSpeed);

		if (playerInput)
		{
			Vector3 movX = Vector3.right * inputX;
			Vector3 movY = Vector3.forward * inputY;
			lastVelocityDirection = (movX + movY).normalized;
		}
		Vector3 movementDelta = lastVelocityDirection * CurrentSpeed;

		_characterControl.Move(movementDelta);

		if (movementDelta != Vector3.zero)
		{
			var newLookRotation = new Quaternion();
			newLookRotation.SetLookRotation(movementDelta.normalized);
			_characterControl.transform.rotation = Quaternion.Lerp(_characterControl.transform.rotation, newLookRotation, RotateSpeed * deltaTime);
		}

		animator.SetFloat ("Speed_Blend", CurrentSpeed/MaxSpeed);

	}
}
