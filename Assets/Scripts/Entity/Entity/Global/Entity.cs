﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public abstract class Entity : MonoBehaviour {

    public void Hide() {
		gameObject.SetActive(false);
    }

	public void Destroy() {
		Destroy (gameObject);
	}
}
