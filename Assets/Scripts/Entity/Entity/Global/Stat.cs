﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class Stat {

	[SerializeField]
	private StatController controller;
	[SerializeField]
	private float maxValue, minValue, currentValue;

	public float MaxValue {
		get {
			return maxValue;
		}
		set {
			this.maxValue = value;
			if(controller != null)
				controller.MaxValue = maxValue;
		}
	}

    public float MinValue {
        get {
            return minValue;
        }
        set {
            this.minValue = value;
            if (controller != null)
                controller.MinValue = minValue;
        }
    }

    public float CurrentValue {
		get {
			return currentValue;
		}
		set {
			this.currentValue = Mathf.Clamp(value, 0, MaxValue);
			if(controller != null)
				controller.Value = currentValue;
		}
	}

	public void initialize() {
		this.MaxValue = maxValue;
        this.MinValue = minValue;
        this.CurrentValue = currentValue;
	}
}
