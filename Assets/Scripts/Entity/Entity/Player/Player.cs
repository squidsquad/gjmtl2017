﻿using UnityEngine;
using System.Collections;

public class Player : ControllableEntity {

	[SerializeField]
	private Stat health;
	[SerializeField]
	private Stat breath;
	[SerializeField]
	private Color color;
	[SerializeField]
	private GameObject mouth;
	[SerializeField]
	private GameObject circle;
	[SerializeField]
	private GameObject statsUI;
    [SerializeField]
    private int breathRegenRate = 5;
    [SerializeField]
	private ShieldSpawner _shieldSpawner;
	[SerializeField]
	private BulletSpawner _bulletSpawner;
	[SerializeField]
	private Rigidbody _deathRigidBody;
	[SerializeField]
	PlayerAudio _playerAudio;

	public Color Color { 
		get { return color; } 
		set { 
			this.color = value;
			mouth.GetComponent<Renderer> ().materials[1].color = value;
			circle.GetComponent<Renderer> ().material.color = value;
		}
	}

	public Stat Health { get { return health; }	}

	public Stat Breath { get { return breath; }	}

	void Awake() {
		health.initialize ();
		breath.initialize ();

        _bulletSpawner.SetAmmo(Breath);
        _shieldSpawner.SetBreathStat(Breath);
	}

    override protected void Update()
    {
        breath.CurrentValue += breathRegenRate * Time.deltaTime;

		base.Update();
    }

    public override void defense1 ()
	{
		SpawnShield(AttackType.GREEN);
	}

	public override void defense2 ()
	{
		SpawnShield(AttackType.RED);
	}

	public override void defense3 ()
	{
		SpawnShield(AttackType.BLUE);
	}

	public override void defense4 ()
	{
		SpawnShield(AttackType.ORANGE);
	}

	public override void attack ()
	{
        _bulletSpawner.LaunchBullet();
	}

	private void SpawnShield(AttackType attackType)
	{
        _shieldSpawner.SpawnShield(attackType);
	}

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.gameObject.layer == 8)
        {
            var bullet = collision.collider.GetComponent<Bullet>();
            if (bullet)
            {
				animator.SetTrigger ("Hurt_Trig");

				_playerAudio.PlayHurt();

                Health.CurrentValue -= bullet.Damage;
				if (Health.CurrentValue <= 0)
				{
					playerDied(collision.contacts[0].normal, bullet.Damage);
                }
            }
        }
    }

    private void playerDied(Vector3 torque, int killingDamage)
    {
        enabled = false;
		statsUI.SetActive(false);
		_deathRigidBody.isKinematic = false;
		_deathRigidBody.AddTorque(torque * killingDamage * 500);
    }
}
