﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Tags;

public class ConversationManager : MonoBehaviour {

    [SerializeField]
    private UIController uiControl;
    [SerializeField]
    private Text textField;
    [SerializeField]
    private TextAsset file;
    [SerializeField]
    private string[] textLines;
    [SerializeField]
    private int currentLine, endAtLine;
    [SerializeField]
    private float typingSpeed = 0.018f;
    private bool talking, typing, cancelTyping;

    public bool Talking {
        get { return talking; }
        set { talking = value; }
    }

    void Start () {
        if (file != null) 
            textLines = (file.text.Split('\n'));
        if(endAtLine == 0 || endAtLine > textLines.Length - 1) 
            endAtLine = textLines.Length - 1;
    }

    public void startConversation() {
		UIController.Instance.showUI (UITags.ConversationUI.ToString());
        talking = true;
        currentLine = 0;
    }

    public void speak() {
        updateSpeakText();
        ++currentLine;
        updateSpeakStatus();
    }

    public void scrollSpeak() {
        if (!typing) {
            updateScrollSpeakStatus();
            ++currentLine;
        } else if(typing && !cancelTyping) {
            cancelTyping = true;
        }
    }

    public void endConversation() {
		UIController.Instance.showUI (UITags.PlayingUI.ToString());
        talking = false;
    }

    private void updateSpeakText() {
        if (currentLine < textLines.Length - 1)
            textField.text = textLines[currentLine];
    }

    private void updateSpeakStatus() {
        if (currentLine > endAtLine) {
            endConversation();
        }
    }

    private IEnumerator textScroll(string lineOfText) {
        int letter = 0;
        textField.text = "";
        typing = true;
        cancelTyping = false;
        while (typing && !cancelTyping && (letter < lineOfText.Length - 1)) {
            textField.text += lineOfText[letter];
            ++letter;
            yield return new WaitForSeconds(typingSpeed);
        }
        textField.text = lineOfText;
        typing = false;
        cancelTyping = false;
    }

    private void updateScrollSpeakStatus() {
        if (currentLine > endAtLine) {
            endConversation();
        } else {
            typing = true;
            StartCoroutine(textScroll(textLines[currentLine]));
        }
    }
}
