﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class Trigger : MonoBehaviour {

	[SerializeField]
	private GameObject target;
	[SerializeField]
	private int maxTriggerCount = -1;
    [SerializeField]
    private bool needInteraction;
    private int triggerCount;
	private List<GameObject> triggerrer = new List<GameObject>();

	public int TriggerCount {
		get { return TriggerCount; }
		set { triggerCount = value; }
	}

	public abstract void enterTrigger (GameObject target);
    public abstract void exitTrigger (GameObject target);


    void Update() {
		if(needInteraction && Input.GetButtonDown ("Interact") && triggerrer.Count > 0) {
			enterTrigger (target);
			++triggerCount;
		}
	}

	public void OnTriggerEnter (Collider other) {
		if (target != null && (triggerCount < maxTriggerCount || maxTriggerCount == -1) && !needInteraction) {
			enterTrigger (target);
			++triggerCount;
		}
		triggerrer.Add (other.gameObject);
	}

	public void OnTriggerExit(Collider other) {
		if(triggerrer.Contains(other.gameObject))
			triggerrer.Remove (other.gameObject);
        exitTrigger(target);
	}
}
