﻿using UnityEngine;
using System.Collections;
using System;

[RequireComponent(typeof(ConversationManager))]
public class TriggerConversation : Trigger {

    [SerializeField]
    private Mode mode;
    private ConversationManager textManager;

    public enum Mode {
        Speak, 
        ScrollSpeak
    }

    void Awake () {
        textManager = GetComponent<ConversationManager>();
    }

    public override void enterTrigger(GameObject target) {
        switch (mode) {
            case Mode.Speak: speak(); break;
            case Mode.ScrollSpeak: scrollSpeak(); break;
        }
    }

    public override void exitTrigger(GameObject target) {
        if (textManager.Talking)
            textManager.endConversation();
    }

    private void speak() {
        if (!textManager.Talking)
            textManager.startConversation();
        textManager.speak();
    }

    private void scrollSpeak() {
        if (!textManager.Talking)
            textManager.startConversation();
        textManager.scrollSpeak();
    }
}
