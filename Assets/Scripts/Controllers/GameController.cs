﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Tags;

[RequireComponent (typeof (MultiplayerController))]
public class GameController : MonoBehaviour {

	[SerializeField]
	private Text winningLabel;
	[SerializeField]
	private Text winningMessage;
	private string WINNING = "Congratz ";
	private string WINNING_MESSAGE = "The other banshees are now deaf!" +
		"\n\n\nPress START to restart the game!" +
		"\nPress SELECT to quit to menu!";
	private string ALONE_MESSAGE = "You are a sad little banshee and have no friends. T.T" +
		"\n\n\nPress START to restart the game!" +
		"\nPress SELECT to quit to menu!";
    private State currentState;
    private UIController uiControl;
	private MultiplayerController multiplayerControl;

    public static GameController Instance {
        get;
        private set;
	}

	private enum State {
		Paused,
		Selecting,
		Playing,
		Ended
	}

    void Awake() {
        if (Instance != null && Instance != this)
            Destroy(gameObject);
        Instance = this;
    }

	void Start() {
		multiplayerControl = GetComponent<MultiplayerController> ();
		WaitingForPlayer ();
    }

	void Update() {
		if (currentState == State.Selecting) {
			multiplayerControl.CheckForConnection ();
			if (multiplayerControl.CheckMinimumPlayerReached () && Input.GetButtonDown ("Start"))
				InitializeGame ();
			if(Input.GetButtonDown("Quit")) 
				QuitGame();
		} else {
			if (currentState == State.Playing && Input.GetButtonDown("Quit")) {
				QuitGame();
			}
            if (currentState == State.Paused && Input.GetButtonDown("Quit"))
            {
                QuitGame();
            }
            if (Input.GetButtonDown("Pause") && currentState != State.Ended) {
				if (currentState == State.Paused)
					ResumeGame();
				else if (currentState == State.Playing)
					PauseGame();
			}
			checkEndGame();
			if (currentState == State.Ended) {
				if(Input.GetButtonDown("Replay")) 
					RestartScene();
				if(Input.GetButtonDown("Quit")) 
					QuitGame();
			}
		}
	}

	private void WaitingForPlayer() {
		UIController.Instance.showUI (UITags.CharacterSelectionUI.ToString());
		currentState = State.Selecting;
		Time.timeScale = 1.0f;
	}

	private void InitializeGame() {
		UIController.Instance.showUI (UITags.PlayingUI.ToString());
		multiplayerControl.InitializePlaying ();
		currentState = State.Playing;
		Time.timeScale = 1.0f;
	}

	private void ResumeGame() {
		UIController.Instance.showUI (UITags.PlayingUI.ToString());
		currentState = State.Playing;
		Time.timeScale = 1.0f;
	}

	private void PauseGame() {
		UIController.Instance.showUI (UITags.PauseUI.ToString ());
		SetPauseState ();
	}

    private void SetPauseState() {
		currentState = State.Paused;
		Time.timeScale = 0.0f;
	}

	private void RestartScene() {
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

	private void QuitGame() {
		SceneManager.LoadScene(0);
	}

	private void checkEndGame() {
		if (multiplayerControl.isAllPlayerDead ())
			EndGame (multiplayerControl.GetRemainingPlayer().ControllerNumber);
	}

	public void EndGame(int playerNumber) {
		UIController.Instance.showUI (UITags.EndingUI.ToString ());
		winningLabel.text = WINNING + "player " + playerNumber + "!";
		if(multiplayerControl.isPlayerAlone()) {
			winningMessage.text = ALONE_MESSAGE;
		} else {
			winningMessage.text = WINNING_MESSAGE;
		}
        currentState = State.Ended;
	}
}
