﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPointController : MonoBehaviour {

	[SerializeField]
	private GameObject playersContainer;

	public void SpawnPlayers(List<GameObject> players)
	{
		for (int i = 0; i < players.Count; i++) {
			GameObject player = players [i];
			PlacePlayer (player, i);
			player.gameObject.SetActive (true);
		}
	}

	private void PlacePlayer(GameObject player, int index) {
		GameObject[] spawns = GameObject.FindGameObjectsWithTag("Spawn");
		if (index < spawns.Length) {
			player.transform.position = spawns [index].transform.position;
			player.transform.parent = playersContainer.transform;
		}
	}
}
