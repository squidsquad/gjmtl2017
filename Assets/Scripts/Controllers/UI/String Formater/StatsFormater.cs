﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class StatsFormater {

	public FormatType Format;
	[SerializeField]
	private string startText;
	[SerializeField]
	private string endText;
	[SerializeField]
	private string separator;

	public enum FormatType {
		Proportion,
		Label
	}

	public string formatTextWithLabel(float value) {
		return startText + value + endText;
	}

	public string formatTextWithProportion(float value, float maxValue) {
		return value + separator + maxValue;
	}
}
