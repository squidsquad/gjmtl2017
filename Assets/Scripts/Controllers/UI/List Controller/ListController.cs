﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ListController : MonoBehaviour {

	[SerializeField]
	private Color selectedColor;
	private List<ListItem> items;
	private int selected;

	void Awake() {
		items = new List<ListItem> ();
		foreach (Transform item in transform) {
			items.Add (item.gameObject.GetComponent<ListItem>() as ListItem);
		}
	}

	void Start() {
		selected = 0;
		items [selected].Select (selectedColor);
	}

	void Update() {
		if (Input.GetButtonDown ("Horizontal_P1")) {
			if (Input.GetAxis ("Horizontal_P1") < 0) {
				if (selected > 0) {
					items [selected].Deselect ();
					--selected;
					items [selected].Select (selectedColor);
				}
			} else {
				if (selected < items.Count - 1) {
					items [selected].Deselect ();
					++selected;
					items [selected].Select (selectedColor);
				}
			}
		}
	}
}
