﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ListItem : MonoBehaviour {

	private Image imageComponent;

	void Awake() {
		imageComponent = GetComponent<Image>() as Image;
	}

	public void Select(Color selectedColor) {
		imageComponent.color = selectedColor;
	}

	public void Deselect() {
		Color c = imageComponent.color;
		c.a = 0;
		imageComponent.color = c;
	}
}
