﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MultiplayerUIController : MonoBehaviour {

	public static MultiplayerUIController Instance {
		get;
		private set;
	}

	void Awake() {
		if(Instance != null && Instance != this)
			Destroy(gameObject);
		Instance = this;
	}

	public void displayPlayersUIs(int playerCount) {
		hideAllPlayersUIs ();
		for (int i = 0; i < transform.childCount - 1 && i < playerCount; ++i) {
			transform.GetChild (i).gameObject.SetActive (true);
		}
	}

	private void hideAllPlayersUIs() {
		foreach (Transform ui in transform) {
			ui.gameObject.SetActive(false);
		}
	}
}
