﻿using System;
using System.Collections;
using UnityEngine;

public class UIController : MonoBehaviour {

	public static UIController Instance {
		get;
		private set;
	}

	void Awake() {
		if(Instance != null && Instance != this)
			Destroy(gameObject);
		Instance = this;
	}

    public void showUI(string tag) {
        hideAllInterfaces();
		foreach (Transform ui in transform) {
            if (ui.tag == tag) {
				ui.gameObject.SetActive(true);
            }
        }
    }

    private void hideAllInterfaces() {
		foreach (Transform ui in transform) {
			ui.gameObject.SetActive(false);
		}
	}
}
