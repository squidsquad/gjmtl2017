﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LobbyItem : MonoBehaviour {

	[SerializeField]
	private Text text;
	[SerializeField]
	private GameObject mouth;
	[SerializeField]
	private Animator animator;
	[SerializeField]
	private Image beam;

	public void SetGameObject(GameObject mouth) {
		this.mouth = mouth;
	}

	public void Enable(Color color) {
		text.color = color;
		gameObject.SetActive (true);
		mouth.GetComponent<Renderer> ().materials[1].color = color;
		beam.color = color;
		animator.SetTrigger ("Attack_Trig");
		GetComponent<AudioSource> ().Play ();
	}

	public void Disable() {
		gameObject.SetActive (false);
	}

	public bool IsEnabled() {
		return gameObject.activeSelf;
	}
}
