﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LobbyController : MonoBehaviour {

	[SerializeField]
	private Text selectingText;
	[SerializeField]
	private Camera lobbyCamera;
	private const string MINIMUM_NOT_REACHED = "PRESS " + keyToPress + " TO FIGHT";
	private const string MINIMUM_REACHED = "PRESS START TO FIGHT";
	[SerializeField]
	private const string keyToPress = "A";
	[SerializeField]
	private List<Color> colors = new List<Color> { 
		new Color(0.15f, 0.50f, 0.72f, 0.5f), 
		new Color(0.55f, 0.26f, 0.67f, 0.5f), 
		new Color(0.95f, 0.61f, 0.70f, 0.5f), 
		new Color(0.75f, 0.22f, 0.16f, 0.5f)
	};

	void Start() {
		foreach (Transform item in transform) {
			(item.gameObject.GetComponent<LobbyItem> () as LobbyItem).Disable ();
		}
	}

	public int CheckPlayerConnection(int maxPlayers) {
		for (int i = 0; i < maxPlayers; i++) {
			LobbyItem item = (transform.GetChild (i).GetComponent<LobbyItem> () as LobbyItem);
			if (Input.GetButtonDown(keyToPress + "_P" + (i + 1)) && !item.IsEnabled()) {
				item.Enable (colors[i]);
				return i + 1;
			}
		}
		return 0;
	}

	public void shutdownLobby() {
		lobbyCamera.gameObject.SetActive (false);
	}

	public void SetMinimumNotReached() {
		selectingText.text = MINIMUM_NOT_REACHED;
	}

	public void SetMinimumPlayerReached() {
		selectingText.text = MINIMUM_REACHED;
	}

	public Color GetPlayerColor(int index) {
		return colors[index];
	}
}
