﻿using UnityEngine;
using System.Collections;

public abstract class StatController : MonoBehaviour {

	[SerializeField]
	private StatsFormater textFormat;

	public abstract float Value { set; }

	public float MaxValue { get; set; }

    public float MinValue { get; set; }

    public string formatText(float value) {
		switch (textFormat.Format) {
			case StatsFormater.FormatType.Proportion:
				return textFormat.formatTextWithProportion (value, MaxValue);
			case StatsFormater.FormatType.Label:
				return textFormat.formatTextWithLabel (value);
			default: 
				return value.ToString ();
		}
	}
}
