﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BarController : StatController {

	[SerializeField]
	private Image content;
	[SerializeField]
	private Text valueText;
	[SerializeField]
	private float speed;
	[SerializeField]
	private Color lowColor;
	[SerializeField]
	private Color fullColor;
	[SerializeField]
	private bool lerpColors;

	private float fillAmount;

	public override float Value {
		set {
			if (valueText != null) valueText.text = formatText (value);
			fillAmount = calculateFillAmount(value, MinValue, MaxValue);
		}
	}

	void Start() {
		content.color = fullColor;
	}

	void Update () {
		handleBar ();
	}

	private void handleBar() {
		if (content.fillAmount != fillAmount) {
			content.fillAmount = Mathf.Lerp(content.fillAmount, fillAmount, Time.deltaTime * speed);
		}
		if (lerpColors)
			content.color = Color.Lerp (lowColor, fullColor, fillAmount);
	}

	private float calculateFillAmount(float value, float min, float max) {
		return (value - min) / (max - min);
	}
}
