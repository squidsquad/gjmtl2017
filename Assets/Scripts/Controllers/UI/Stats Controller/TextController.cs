﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextController : StatController {

	[SerializeField]
	private Text valueText;

	public override float Value {
		set {
			valueText.text = formatText (value);
		}
	}
}
