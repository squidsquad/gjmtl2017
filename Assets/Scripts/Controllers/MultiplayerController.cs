﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (SpawnPointController))]
public class MultiplayerController : MonoBehaviour {

	[SerializeField]
	private GameObject playerPrefab;
	[SerializeField]
	private List<GameObject> players;
	[SerializeField]
	private LobbyController lobby;
	[SerializeField]
	private int maxPlayer = 4, minPlayer = 1;
	private Player remainingPlayer;

	public void CheckForConnection() {
		int playerConnecting = lobby.CheckPlayerConnection (maxPlayer);
		if (playerConnecting != 0) {
			AddPlayer (playerConnecting, lobby.GetPlayerColor(playerConnecting - 1));
		}
	}

	public bool CheckMinimumPlayerReached() {
		if (players.Count >= minPlayer) {
			lobby.SetMinimumPlayerReached();
			return true;
		} else {
			lobby.SetMinimumNotReached();
			return false;
		}
	}

	public void InitializePlaying() {
		lobby.shutdownLobby ();
		(GetComponent<SpawnPointController> () as SpawnPointController).SpawnPlayers (players);
        (GetComponent<CameraController>() as CameraController).setTrackedTransforms(players);
    }

	public bool isAllPlayerDead() {
		int playerAlive = 0;
		foreach(GameObject player in players) {
			if (((player.gameObject.GetComponent<Player> () as Player).Health.CurrentValue > 0)) {
				++playerAlive;
				if(playerAlive > 1) 
					return false;
				else
					remainingPlayer = player.gameObject.GetComponent<Player> () as Player;
			}
		}
		return true;
	}

	public bool isPlayerAlone() {
		return players.Count == 1;
	}

	public Player GetRemainingPlayer() {
		return remainingPlayer;
	}

	private void AddPlayer(int playerController, Color color) {
		if (players.Count < maxPlayer) {
			GameObject player = Instantiate(playerPrefab, playerPrefab.transform.position, playerPrefab.transform.rotation);
			players.Add (player);
			(player.GetComponent<Player>() as Player).ControllerNumber = playerController;
			(player.GetComponent<Player> () as Player).Color = color;
			player.SetActive (false);
		}
	}
}
