﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    [SerializeField]
    private GameObject cameraCenter;
    [SerializeField]
    private float ponderationCameraOffset = 1;
    private List<Transform> trackedTransforms = new List<Transform>();
    
    void Update ()
    {
        if (trackedTransforms.Count != 0)
        {
            Vector3 sum = Vector3.zero;
            foreach (var transform in trackedTransforms)
            {
                sum += transform.position;
            }
            Vector3 averagePlayerPosition = sum / trackedTransforms.Count;
            Vector3 offset = (averagePlayerPosition - cameraCenter.transform.position) * ponderationCameraOffset;


            Quaternion rotation = new Quaternion(Camera.main.transform.rotation.x, Camera.main.transform.rotation.y, Camera.main.transform.rotation.z, Camera.main.transform.rotation.w);
            rotation.SetLookRotation(offset - Camera.main.transform.position);
            Camera.main.transform.rotation = rotation;
        }
    }

    public void setTrackedTransforms(List<GameObject> newTrackedTransforms)
    {
        foreach (GameObject item in newTrackedTransforms)
        {
            trackedTransforms.Add(item.transform);
        }
    }

}
